'use strict'

var port = 4080

var server = require('http').createServer()
var path = require('path')
var express = require('express')
var swig = require('swig')
var bodyParser = require('body-parser')

var WebSocketServer = require('ws').Server
var wss = new WebSocketServer({ server: server })
var app = express()

app.engine('html', swig.renderFile)

app.set('view engine', 'html')
app.set('views', path.join(__dirname, '/views'))

app.set('view cache', false)
swig.setDefaults({ cache: false })

app.use('/public', express.static(path.join(__dirname, '/public')))

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.render('index', { msg: 'hello asdf' })
})

var channels = []

app.post('/create-channel', (req, res) => {
  let channelName = req.body.channelName
  channels.push({ name: channelName })
  res.redirect('/' + channelName)
})

app.get('/:channelName', (req, res) => {
  let channel = findChannel(channels, req.params.channelName)
  console.log(channel)
  if (channel) {
    res.render('channel', {
      name: channel.name,
      videoId: channel.videoId,
      lastCommand: channel.lastCommand
    })
  } else {
    res.status(404).send('Not found')
  }
})

wss.on('connection', (ws) => {
  // var location = url.parse(ws.upgradeReq.url, true)
  // you might use location.query.access_token to authenticate or share sessions
  // or ws.upgradeReq.headers.cookie (see http://stackoverflow.com/a/16395220/151312)

  ws.on('message', function incoming (message) {
    let msg = JSON.parse(message)
    let channel = findChannel(channels, msg.channelName)
    if (channel) {
      switch (msg.type) {
        case 'video':
          channel.videoId = msg.videoId
          break
        case 'command':
          console.log('command received: ' + msg.command)
          channel.lastCommand = {
            command: msg.command,
            timestamp: msg.timestamp,
            time: msg.time
          }
          wss.clients.forEach(function (client) {
            client.send(message)
          })
          break
      }
    }
  })
})

function findChannel (channels, name) {
  return channels.find(c => c.name === name)
}

server.on('request', app)
server.listen(port, function () { console.log('Listening on ' + server.address().port) })
